from datetime import datetime as dt


def write(text, file="/var/log/webmark-tool-qt/log.txt"):
    try:
        with open(file, 'a') as f:
            f.write(str(dt.now()))
            f.write(text)
            f.write("\n")
    except Exception as e:
        print(e)

