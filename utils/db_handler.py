import sqlite3

from utils import handler
from utils import standards


con = sqlite3.connect("webmark_db.sqlite")


def has_field_changed(field):
    return field is not None and field != '%same' 


def commit():
    cur = con.cursor()

    try:
        cur.execute("""CREATE TABLE webmarks
                    AS SELECT * FROM webmarks_temp""")
    except sqlite3.OperationalError as e:
        print(e)

    try:
        cur.execute("""DELETE FROM webmarks""")

        cur.execute("""INSERT INTO webmarks SELECT * FROM webmarks_temp""")

        con.commit()

    except sqlite3.OperationalError as e:
        print(e)

    cur.close()


def fetch():
    try:
        close()
    except sqlite3.OperationalError as e:
        print(e)

    try:
        init()
    except sqlite3.OperationalError as e:
        print(e)


def close():
    cur = con.cursor()

    cur.execute("""DROP TABLE webmarks_temp""")

    con.commit()
    cur.close()
    

def init():
    cur = con.cursor()

    try:
        cur.execute("""CREATE TABLE webmarks_temp
                    AS SELECT * FROM webmarks""")

        cur.execute("""DELETE FROM webmarks_temp""")

        cur.execute("""INSERT INTO webmarks_temp SELECT * FROM webmarks""")

    except sqlite3.OperationalError as e:
        print(e)

    con.commit()
    cur.close()


def add_webmarks(patterns):
    cur = con.cursor()

    if options is None:
        options = ""

    cur.executemany(f"""INSERT INTO webmarks_temp(identifier, address, name, browser, options)
                VALUES(?, ?, ?, ?, ?)""", patterns)

    con.commit()
    cur.close()


def add_webmark(identifier, address, name, browser, options):
    cur = con.cursor()

    if options is None:
        options = ""

    cur.execute(f"""INSERT INTO webmarks_temp(identifier, address, name, browser, options)
                VALUES('{identifier}', '{address}', '{name}', '{browser}', '{options}')""")

    con.commit()
    cur.close()


def get_ids_by_pattern(**pattern):
    cur = con.cursor()

    query = """SELECT identifier
            FROM webmarks_temp
            WHERE """
    query += ", ".join([f"""{key}='{value}'""" for key, value in pattern.items() if has_field_changed(value)])

    out = tuple(map(lambda x: x[0], cur.execute(query).fetchall()))
    
    cur.close()

    return out


def remove_webmarks(identifiers):
    cur = con.cursor()

    mapped_identifiers = ", ".join(map(lambda x: f"'{x}'", identifiers))
    cur.execute(f"""DELETE FROM webmarks_temp
                WHERE identifier in ({mapped_identifiers})""")

    con.commit()
    cur.close()


def change_webmarks(identifiers, pattern):
    cur = con.cursor()

    # write users changes
    query = """UPDATE webmarks_temp
            SET """
    query += ", ".join([f"{key}='{value}'" for key, value in pattern.items() if has_field_changed(value)])
    # magic
    mapped_identifiers = ", ".join(map(lambda x: f"'{x}'", identifiers))
    query += f" WHERE identifier in ({mapped_identifiers})"

    cur.execute(query)

    # update identifiers
    # create pattern to change identifiers
    query = f"""SELECT * FROM webmarks_temp
            WHERE identifier in ({mapped_identifiers})"""

    res = cur.execute(query)
    data = res.fetchall()
    keys = list(map(lambda x: x[0], res.description))
    # magic
    identifiers = list(map(lambda x: (standards.create_identifier(
        dict(zip(keys[1:], x[1:]))), x[0]), 
               data))

    query = f"""UPDATE webmarks_temp
            SET identifier=?
            WHERE identifier=?"""

    cur.executemany(query, identifiers)

    con.commit()
    cur.close()


def get_current():
    cur = con.cursor()

    out = cur.execute("""SELECT *
                    FROM webmarks""").fetchall()
    
    cur.close()

    return out


def get_temportary():
    cur = con.cursor()

    out = cur.execute("""SELECT *
                      FROM webmarks_temp""").fetchall()
    
    cur.close()

    return out


def get_olds_dicts():
    cur = con.cursor()

    try:
        temportary = cur.execute("""SELECT * FROM
                                 webmarks_temp""").fetchall()
    except sqlite3.OperationalError as e:
        temportary = []
        print(e)

    try:
        current = cur.execute("""SELECT * FROM
                              webmarks""").fetchall()
    except sqlite3.OperationalError as e:
        current = []
        print(e)

    res = cur.execute("""SELECT * FROM
                      webmarks""")
    keys = tuple(map(lambda x: x[0], res.description))
    values = set(current) - set(temportary)

    out = list(map(lambda x: dict(zip(keys, x)), values))

    cur.close()

    return out


def get_olds_ids():
    cur = con.cursor()

    try:
        temportary = cur.execute("""SELECT identifier FROM
                                 webmarks_temp""").fetchall()
    except sqlite3.OperationalError as e:
        temportary = []
        print(e)

    try:
        current = cur.execute("""SELECT identifier FROM
                              webmarks""").fetchall()
    except sqlite3.OperationalError as e:
        current = []
        print(e)

    out = list(map(lambda x: x[0], set(current) - set(temportary)))

    cur.close()

    return out


def get_news_dicts():
    cur = con.cursor()

    try:
        temportary = cur.execute("""SELECT * FROM
                                 webmarks_temp""").fetchall()
    except sqlite3.OperationalError as e:
        temportary = []
        print(e)

    try:
        current = cur.execute("""SELECT * FROM
                              webmarks""").fetchall()
    except sqlite3.OperationalError as e:
        current = []
        print(e)

    res = cur.execute("""SELECT * FROM
                      webmarks""")
    keys = tuple(map(lambda x: x[0], res.description))
    values = set(temportary) - set(current)

    out = list(map(lambda x: dict(zip(keys, x)), values))

    cur.close()

    return out


def get_news_ids():
    cur = con.cursor()

    try:
        temportary = cur.execute("""SELECT identifier FROM
                                 webmarks_temp""").fetchall()
    except sqlite3.OperationalError as e:
        temportary = []
        print(e)

    try:
        current = cur.execute("""SELECT identifier FROM
                              webmarks""").fetchall()
    except sqlite3.OperationalError as e:
        current = []
        print(e)

    out = list(map(lambda x: x[0], set(temportary) - set(current)))

    cur.close()

    return out

