def create_identifier(kvargs):
    pattern = ("browser", "options", "name", "address")
    args = tuple(map(lambda x: kvargs[x], pattern))

    out = "webmark_" + "_".join(args)

    return out

