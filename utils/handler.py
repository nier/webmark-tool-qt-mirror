from utils import db_handler as db
from utils import log_handler as log
from utils import webmark_handler as wm
from ui import messages


def excepted(func):
    def out(*args, **kvargs):
        try:
            if args == (False,):
                args = tuple()
            func(*args, **kvargs)
        except Exception as e:
            messages.show_error(e)
            log.write(str(e))
    return out


@excepted
def change_webmarks(pattern_search, pattern_set):
    ids = db.get_ids_by_pattern(**pattern_search)
    db.change_webmarks(ids, pattern_set)


@excepted
def add_webmarks(patterns):
    db.add_webmarks(patterns)


@excepted
def add_webmark(**kvargs):
    db.add_webmark(**kvargs)


@excepted
def remove_webmarks(**kvargs):
    ids = db.get_ids_by_pattern(**kvargs)
    db.remove_webmarks(ids)


@excepted
def commit():
    # remove old
    ids_to_remove = db.get_olds_ids()
    wm.remove_webmarks(ids_to_remove)

    # add new
    news = db.get_news_dicts()
    wm.add_webmarks(news)
    
    text = f"""
    commited to main table.
    removed:
    {db.get_olds_dicts()}
    added:
    {db.get_news_dicts()}
    """
    log.write(text)

    db.commit()


@excepted
def fetch():
    db.fetch()

   
