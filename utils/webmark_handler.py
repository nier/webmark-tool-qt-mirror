from subprocess import run
from os import chdir

def add_webmark(identifier, address, name, browser, options):
    chdir("./webmark-tool")
    script = ["bash", "add_webmark.sh"]
    if not address:
        return False
    
    if not name:
        name = address

    if not browser:
        browser = "firefox"

    script.extend(["-b", browser])
    script.extend(["-n", name])
    script.extend(["-a", address])
    script.extend(["-i", identifier])

    if options != "" and options is not None:
        script.extend(["-o", options])

    run(script)
    chdir("..")


def add_webmarks(patterns):
    for kvargs in patterns:
        print(kvargs)
        add_webmark(**kvargs)


def remove_webmarks(ids):
    chdir("./webmark-tool")
    
    for i in ids:
        run(["bash", "remove_webmark.sh", "-i", i])

    chdir("..")


