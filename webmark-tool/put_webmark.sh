#!/bin/bash

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "util to put webmark (executable link)"
      echo "\n-h --help 	    show brief help"
      echo "\n-n --name 	    name of the webmark"
      echo "\n-m --mark		    webmark"
      echo "\n-i --identifier	unicue name for files in system"
      exit 1
      ;;
    -n|--name)
      shift
      if test $# -gt 0; then
        export name=$1
      else
        echo "hasnt given name"
	exit 1
      fi
      shift
      ;;
    -m|--mark)
      shift
      if test $# -gt 0; then
        export mark=$1
      else
        echo "hasnt given webmark"
	exit 1
      fi
      shift
      ;;
    -i|--identifier)
      shift
      if test $# -gt 0; then
        export identifier=$1
      else
        echo "hasnt given identifier"
	exit 1
      fi
      shift
      ;;
    *)
      shift
      continue
      ;;
  esac
done

mkdir -p /home/$USER/.config/webmark-tool/bookmarks
mkdir -p /home/$USER/.local/share/applications


app_code="#!/bin/bash\n\n$mark\n"

echo -e $app_code > ~/.config/webmark-tool/bookmarks/$identifier.sh
chmod 755 /home/$USER/.config/webmark-tool/bookmarks/$identifier.sh


desktop_code="[Desktop Entry]\nEnecoding=utf-8\nVersion=1.0\nType=Application\nTerminal=false\nExec=sh /home/$USER/.config/webmark-tool/bookmarks/$identifier.sh\nName=$name\nComment=mark for web\nCategories=Network;WebBrowser;\nIcon=/home/$USER/.config/webmark-tool/icons/$identifier.png"

echo -e $desktop_code > ~/.local/share/applications/$identifier.desktop

