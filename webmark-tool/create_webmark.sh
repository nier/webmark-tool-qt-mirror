#!/bin/bash

source config.sh

export options=""
while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "util to make webmark (executable link)"
      echo "\n-h --help 	show brief help"
      echo "\n-b --browser 	browser, to open the webmark"
      echo "\n-a --address		url address"
      echo "\n-o --options	options, to give it to browser"
      exit 1
      ;;
    -b|--browser)
      shift
      if test $# -gt 0; then
        export browser=$1
      else
        echo "hasnt given browser"
	exit 1
      fi
      shift
      ;;
    -o|--options)
      shift
      if test $# -gt 0; then
        export options=$1
      else
        echo "hasnt given options"
	exit 1
      fi
      shift
      ;;
    -a|--address)
      shift
      if test $# -gt 0; then
        export addr=$1
      else
        echo "hasnt given address"
	exit 1
      fi
      shift
      ;;
    *)
      shift
      continue
      ;;
  esac
done

echo -e ${patterns[$browser]}

