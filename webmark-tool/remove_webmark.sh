#!/bin/bash

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo -e "util to remove webmark (executable link)"
      echo -e "\n-h --help 	show brief help"
      echo -e "\n-i --identifier	unicue name for files in system"
      exit 1
      ;;
    -i|--identifier)
      shift
      if test $# -gt 0; then
        export identifier=$1
      else
        echo "hasnt given name"
	exit 1
      fi
      shift
      ;;
    *)
      shift
      continue
      ;;
  esac
done


rm ~/.config/webmark-tool/bookmarks/$identifier.sh
rm ~/.config/webmark-tool/icons/$identifier.png
rm ~/.local/share/applications/$identifier.desktop

