#!/bin/bash

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "util to put webmark (executable link)"
      echo "\n-h --help 	    show brief help"
      echo "\n-i --identifier	unicue name for files in system"
      echo "\n-a --addr	url-address"
      exit 1
      ;;
    -a|--addr)
      shift
      if test $# -gt 0; then
        export address=$1
      else
        echo "hasnt given address"
	exit 1
      fi
      shift
      ;;
    -i|--identifier)
      shift
      if test $# -gt 0; then
        export identifier=$1
      else
        echo "hasnt given identifier"
	exit 1
      fi
      shift
      ;;
    *)
      shift
      continue
      ;;
  esac
done


echo "123"
mkdir -p /home/$USER/.config/webmark-tool/icons
echo "/home/$USER/.config/webmark-tool/icons/$identifier.png $address" | python3 load_image.py

