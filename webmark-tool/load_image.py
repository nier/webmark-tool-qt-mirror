import requests
from requests.exceptions import *
from bs4 import BeautifulSoup

from itertools import product


class NoLogoInTitleException(Exception):
    pass


def get_logo(address):
    try:
        page = requests.get(address)
    except requests.exceptions.MissingSchema:
        address = "https://" + address
        page = requests.get(address)

    soup = BeautifulSoup(page.content, 'html.parser')
    links = soup.select("link")

    server = next(filter(lambda x: '.' in x, address.split("/")))
    try:
        location = next(filter(lambda x: 'icon' in x.get("rel")[0], links)).get("href")
        try:
            r = requests.get(location)
        except RequestException as e:
            try:
                r = requests.get("https://" + location)
            except RequestException as e:
                r = requests.get("https://" + server + location)     


    # if no one icon marked as logo, then shall check standard pathes
    except StopIteration:
        sizes = (".", "-32.", "-64.", "-128.", "-180.", "-192.", "-57.", "-76.", "-96.", "-120.", "-144.", "-152.", "-167.", "-195.", "-196.", "-228.")
        suffixes = ("png", "ico", "jpg")
        for si, su in product(sizes, suffixes):
            end = si + su
            location = f"/favicon{end}"
            url = "http://" + server + location
            print(url)
            r = requests.get(url)
            if r.status_code != 404:
                break
        else:
            raise NoLogoInTitleException

    return r


def load_logo(destination, address):
    try:
        r = get_logo(address)
        with open(destination, 'wb') as f:
            f.write(r.content)

    except NoLogoInTitleException:
        with open(destination, 'wb') as out, open("./src/default.png", 'rb') as inp:
            out.write(inp.read())


load_logo(*input().split())

