from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from sqlite3 import OperationalError


class ExceptionDialog(QMessageBox):
    def __init__(self, exception):
        super().__init__()
        self.setWindowTitle("Error")
        print(exception)


class QueryExceptionDialog(ExceptionDialog):
    def __init__(self, exception):
        super().__init__(exception)
        self.setText("probably, you filled text boxes wrong...")
        self.setIcon(QMessageBox.Warning)


class RegularExceptionDialog(ExceptionDialog):
    def __init__(self, exception):
        super().__init__(exception)
        self.setText(str(exception))
        self.setIcon(QMessageBox.Critical)


def show_error(exception):
    if isinstance(exception, OperationalError):
        d = QueryExceptionDialog(exception)
    else:
        d = RegularExceptionDialog(exception)

    d.exec_()

