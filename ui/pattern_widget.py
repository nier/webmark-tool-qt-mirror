from PyQt5.QtSql import QSqlDatabase, QSqlTableModel
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class PatternWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.setup_ui()
        self.show()


    def setup_ui(self):
        self.resize(248, 401)


        self.verticalLayout = QVBoxLayout(self)


        # title
        self.label = QLabel(self)
        self.label.setAlignment(Qt.AlignLeading|Qt.AlignTop)
        self.label.setText("create webmark")
        self.verticalLayout.addWidget(self.label)


        # address box
        self.address_horizontalLayout = QHBoxLayout()

        self.address_label = QLabel(self)
        self.address_label.setFrameShape(QFrame.NoFrame)
        self.address_label.setText("address=")
        self.address_horizontalLayout.addWidget(self.address_label)

        self.address_lineEdit = QLineEdit(self)
        self.address_horizontalLayout.addWidget(self.address_lineEdit)

        self.verticalLayout.addLayout(self.address_horizontalLayout)
        self.address_horizontalLayout.setAlignment(Qt.AlignTop)


        # name box
        self.name_horizontalLayout = QHBoxLayout()

        self.name_label = QLabel(self)
        self.name_label.setText("name=")
        self.name_horizontalLayout.addWidget(self.name_label)

        self.name_lineEdit = QLineEdit(self)

        self.name_horizontalLayout.addWidget(self.name_lineEdit)

        self.verticalLayout.addLayout(self.name_horizontalLayout)
        self.name_horizontalLayout.setAlignment(Qt.AlignTop)


        # browser box
        self.browser_horizontalLayout = QHBoxLayout()

        self.browser_label = QLabel(self)
        self.browser_label.setText("browser=")
        self.browser_horizontalLayout.addWidget(self.browser_label)

        self.browser_lineEdit = QLineEdit(self)
        self.browser_horizontalLayout.addWidget(self.browser_lineEdit)

        self.verticalLayout.addLayout(self.browser_horizontalLayout)
        self.browser_horizontalLayout.setAlignment(Qt.AlignTop)


        # options box
        self.options_horizontalLayout = QHBoxLayout()

        self.options_label = QLabel(self)
        self.options_label.setText("options=")
        self.options_horizontalLayout.addWidget(self.options_label)

        self.options_lineEdit = QLineEdit(self)
        self.options_horizontalLayout.addWidget(self.options_lineEdit)

        self.verticalLayout.addLayout(self.options_horizontalLayout)
        self.options_horizontalLayout.setAlignment(Qt.AlignTop)
        self.verticalLayout.setAlignment(Qt.AlignTop)
        

        self.layouts = [
                self.name_horizontalLayout,
                self.address_horizontalLayout,
                self.browser_horizontalLayout,
                self.options_horizontalLayout]


    def add_widget(self, widget):
        self.layouts.append(QHBoxLayout())
        self.layouts[-1].addWidget(widget)
        self.verticalLayout.addLayout(self.layouts[-1])
        self.layouts[-1].setAlignment(Qt.AlignTop)


    def data(self):
        out = dict()
        out["browser"] = self.browser_lineEdit.text()
        out["options"] = self.options_lineEdit.text()
        out["name"] = self.name_lineEdit.text()
        out["address"] = self.address_lineEdit.text()
        return out


    def enable(self):
        self.setEnabled(True)


    def setText(self, text):
        self.label.setText(text)

