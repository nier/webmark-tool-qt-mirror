import sys

from PyQt5.QtSql import QSqlDatabase, QSqlTableModel
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

from utils import handler
from utils import standards
from ui.pattern_widget import PatternWidget


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setup_ui()


    def setup_ui(self):
        self.move(300, 300)
        self.resize(649, 401)
        self.setWindowTitle("webmark tool")


        # main box
        self.horizontal_layout = QGridLayout(self)
        self.horizontal_layout.setColumnMinimumWidth(1, 600)


        # input box
        self.input_layout = QGridLayout()
        self.input_layout.setColumnMinimumWidth(0, 250)
        

        # handle box
        self.handle_layout = QHBoxLayout()

        self.save_button = QPushButton(self)
        self.save_button.setText("save")
        self.save_button.clicked.connect(handler.commit)

        self.handle_layout.addWidget(self.save_button)


        self.fetch_button = QPushButton(self)
        self.fetch_button.setText("update buffer")
        self.fetch_button.clicked.connect(handler.fetch)
        self.fetch_button.clicked.connect(self.update_table_view)

        self.handle_layout.addWidget(self.fetch_button)


        self.help_button = QPushButton(self)
        self.help_button.setText("help")
        self.help_button.clicked.connect(self.show_help)

        self.handle_layout.addWidget(self.help_button)

        self.handle_layout.setAlignment(Qt.AlignTop)

        self.input_layout.addLayout(self.handle_layout, 0, 0)


        # input actions (tab widget)
        self.actions_layout = QVBoxLayout()

        self.tab_widget = QTabWidget(self)

        self.add_tab = AddPatternWidget(self)
        self.tab_widget.addTab(self.add_tab, "add")
        self.add_tab.ok_button.clicked.connect(self.update_table_view)


        self.remove_tab = RemoveByPatternWidget(self)
        self.tab_widget.addTab(self.remove_tab, "remove")
        self.remove_tab.ok_button.clicked.connect(self.update_table_view)


        self.change_tab = ChangeByPatternWidget(self)
        self.tab_widget.addTab(self.change_tab, "change")
        self.change_tab.ok_button.clicked.connect(self.update_table_view)

        self.tab_widget.setCurrentIndex(0)
        # self.tab_widget.adjustSize()

        self.actions_layout.addWidget(self.tab_widget)
        self.actions_layout.setAlignment(Qt.AlignTop)

        self.input_layout.addLayout(self.actions_layout, 1, 0)
        self.input_layout.setAlignment(Qt.AlignTop)

        self.horizontal_layout.addLayout(self.input_layout, 0, 0)


        # output box
        self.output_layout = QVBoxLayout()
        self.output_layout.setAlignment(Qt.AlignTop)

        self.tableView = QTableView(self)
        self.tableView.setGeometry(QRect(249, 0, 401, 401))
        self.update_table_view()

        self.output_layout.addWidget(self.tableView)

        self.horizontal_layout.addLayout(self.output_layout, 0, 1)
        self.horizontal_layout.setAlignment(Qt.AlignLeft)


        self.show()


    def update_table_view(self):
        self.db = QSqlDatabase.addDatabase('QSQLITE')
        self.db.setDatabaseName('webmark_db.sqlite')
        self.db.open()

        self.model = QSqlTableModel(self, self.db)
        self.model.setTable('webmarks_temp')
        self.model.select()

        self.tableView.setModel(self.model)


    def show_help(self):
        self.hw = HelpWindow()
        self.hw.show()


class AddPatternWidget(PatternWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.ok_button = QPushButton(self)
        self.ok_button.setText("ok")
        self.add_widget(self.ok_button)
        self.ok_button.clicked.connect(
                lambda: 
                handler.add_webmark(**self.data()))


    def data(self):
        out = super().data()
        out["identifier"] = standards.create_identifier(out)
        return out


class RemoveByPatternWidget(PatternWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.setText("DELTE FROM webmarks WHERE ")


        self.name_lineEdit.setText("%same")
        self.address_lineEdit.setText("%same")
        self.browser_lineEdit.setText("%same")
        self.options_lineEdit.setText("%same")


        self.ok_button = QPushButton(self)
        self.ok_button.setText("ok")
        self.add_widget(self.ok_button)
        self.ok_button.clicked.connect(
                lambda: 
                handler.remove_webmarks(**self.data()))


class ChangeByPatternWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)

        self.vertical_layout = QVBoxLayout(self)

        # pattern constructor
        self.pattern_set = PatternWidget(self)
        self.pattern_set.setText("UPDATE webmarks SET")
        self.vertical_layout.addWidget(self.pattern_set)

        self.pattern_set.name_lineEdit.setText("%same")
        self.pattern_set.address_lineEdit.setText("%same")
        self.pattern_set.browser_lineEdit.setText("%same")
        self.pattern_set.options_lineEdit.setText("%same")

        
        # search pattern constructor
        self.pattern_search = PatternWidget(self)
        self.pattern_search.setText("WHERE")
        self.vertical_layout.addWidget(self.pattern_search)

        self.pattern_search.name_lineEdit.setText("%same")
        self.pattern_search.address_lineEdit.setText("%same")
        self.pattern_search.browser_lineEdit.setText("%same")
        self.pattern_search.options_lineEdit.setText("%same")


        self.ok_button = QPushButton(self)
        self.ok_button.setText("ok")
        self.ok_button.clicked.connect(
                lambda: 
                handler.change_webmarks(self.pattern_search.data(), self.pattern_set.data()))

        self.vertical_layout.addWidget(self.ok_button)
        self.vertical_layout.setAlignment(Qt.AlignTop)


class HelpWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("help")
        self.label = QLabel(self)
        self.label.setText("install linux")
        self.label.move(0, 0)


