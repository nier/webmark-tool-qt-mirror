import sys

from ui.main_window import MainWindow
from utils import db_handler
from PyQt5.QtWidgets import QApplication

db_handler.init()

app = QApplication(sys.argv)
MainWindow = MainWindow()
MainWindow.show()
sys.exit(app.exec())
